const controllers = require('controllers');

module.exports = [
    {
        method: 'GET',
        path: '/',
        options: {
            handler: controllers.app.app
        }
    },
    {
        method:'POST',
        path:'/api',
        options: {
            handler: controllers.app.api
        }
    }
]