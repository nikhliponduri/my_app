const axios = require('axios');
const pLimit = require('p-limit');
const fs = require('fs');
const path = require('path');

module.exports.app = async (request, h) => {
  return h.view('app');
};

module.exports.api = async (request, h) => {
  try {
    const rc_numbers = [365410636282, 365410635979, 365410635985,365410636282, 365410635979, 365410635985,365410636282, 365410635979, 365410635985,365410636282, 365410635979, 365410635985];
    const { month, year, ag_token, Cookie, url } = request.payload;
    const limit = pLimit(100);
    const promises = [];
    rc_numbers.forEach(num => {
      promises.push(
        limit(() => axios.get(`${url}?rc_no=${num}&month=${month}&year=${year}&ag_token=${ag_token}`, {
          headers: {
            Cookie
          }
        })));
    });
    const res = await Promise.all(promises);
    const data = [];
    for (const [i, subResponse] of res.entries()) {
      if (!subResponse.data.response_status) {
        return h.response({
          message: subResponse.data.response_msg
        }).code(400)
      }
      if (!Array.isArray(subResponse.data.list1)) {
        throw new Error('List 1 is not a array');
      }
      if (!subResponse.data.list1.length) {
        data.push({
          rc_no: rc_numbers[i],
          name: subResponse.data.list[0].member_name_en,
          i: i + 1
        });
      }
    }
    if (data.length) {
      let dataString = '';
      data.map(obj => {
        dataString += `${obj.i}.  ${obj.name}   ${obj.rc_no} \n`;
      })
      fs.writeFileSync(path.resolve(__dirname, '../../') + `/responses/response${new Date().toISOString()}`, dataString);
    }
    return h.response({ data });
  } catch (error) {
    console.log(error);
    return h.response(error).code(500);
  }
}