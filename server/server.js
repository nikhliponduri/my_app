
require('app-module-path').addPath(__dirname);
const Hapi = require('@hapi/hapi');
const Bell = require('@hapi/bell');
const Vision = require('@hapi/vision');
const Ejs = require('ejs');
const config = require('config');
const Hoek = require('@hapi/hoek');
const Inert = require('@hapi/inert');
const cookie = require('@hapi/cookie');
const Path = require('path');
const routes = require('routes');
const logger = console;
const controllers = require('./controllers');


const server = Hapi.server({
  port: config.port,
  host: config.host,
  routes: {
    files: {
      relativeTo: Path.join(__dirname, '../client/dist/')
    }
  }
});

const init = async () => {
  await server.register([Bell, Vision, Inert, cookie]);

  server.views({
    engines: { ejs: Ejs },
    relativeTo: __dirname,
    path: 'views'
  });

  const allRoutes = Object.values(routes);
  for (const key in allRoutes) {
    server.route(allRoutes[key]);
  }
  server.route({
    method: 'GET',
    path: '/{param*}',
    options: {
      auth: false,
      handler: {
        directory: {
          path: '.',
          redirectToSlash: true,
          index: true
        }
      }
    }
  });

  await server.start();
  logger.info('Server running on 3000');
};

process.on('unhandledRejection', err => {
  console.log('Error on starting Server:', err);
  process.exit(1);
});

init();

