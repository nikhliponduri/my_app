
import React, { useState } from 'react';
import axios from 'axios';
import { ToastsContainer, ToastsStore } from 'react-toasts';


const App = () => {
    const [info, setInfo] = useState({
        rc_no: '',
        month: '',
        year: '',
        ag_token: '',
        Cookie: '',
        url: ''
    });
    const [loading, setLoading] = useState(false);
    const [responses, setResponses] = useState([]);
    const handleSubmit = () => {
        setLoading(true);
        setResponses([]);
        axios.post('/api', info).then(res => {
            setResponses(res.data.data);
            ToastsStore.success('Success');
        }).catch(err => {
            setResponses([]);
            ToastsStore.error("Error");
        }).finally(() => {
            setLoading(false)
        })
    }
    console.log(responses)
    return (
        <>
            <div className='container' style={{ margin: '30px' }}>
                <form className='form-inline'>
                    <div class="form-group">

                        <label>month</label>
                        <select onChange={({ target: { value: month } }) => setInfo({ ...info, month })} value={info.month}>
                            {
                                new Array(12).fill(undefined).map((i, j) => <option value={j}>{j}</option>)
                            }
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Year</label>
                        <input value={info.year} type='number' onChange={({ target: { value: year } }) => setInfo({ ...info, year })} />
                    </div>
                    <div class="form-group">
                        <label>Ag_token</label>
                        <input value={info.ag_token} onChange={({ target: { value: ag_token } }) => setInfo({ ...info, ag_token })} />
                    </div>
                </form>
                <form className='form-inline'>
                    <div class="form-group">
                        <label>Cookie</label>
                        <input value={info.Cookie} onChange={({ target: { value: Cookie } }) => setInfo({ ...info, Cookie })} />
                    </div>
                    <div class="form-group">
                        <label>URL</label>
                        <input value={info.url} onChange={({ target: { value: url } }) => setInfo({ ...info, url })} />
                    </div>
                </form>
                <button disabled={loading} onClick={handleSubmit} type="submit" class="btn btn-primary">Submit</button>
                <div style={{ marginTop: '10px' }}>
                    {responses.map(response => <>
                        <div style={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-between' }}>
                            <p>{response.i}.</p>
                            <p>{response.name}</p>
                            <p>{response.rc_no}</p>
                        </div>
                        <hr />
                    </>)}
                </div>
                <ToastsContainer store={ToastsStore} />
            </div>
        </>
    )
}

export default App

